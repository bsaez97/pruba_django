from django.shortcuts import render

from .forms import RegForm
from .models import Registrado

# Create your views here.
def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        
        ###PRIMERA FORMA DE GUARDAR ARCHIVOS EN BD###
        #inNombre = form_data.get("nombre")
        #inEmail = form_data.get("email")
        #inRegistrado = Registrado.objects.create(nombre=inNombre,email=inEmail)
        
        ###SEGUNDA FORMA DE GUARDAR ARCHIVOS EN BD###
        inRegistrado = Registrado()
        inRegistrado.nombre = form_data.get("nombre")
        inRegistrado.email = form_data.get("email")
        inRegistrado.save();

    context = {
        "el_form": form,
    }
    return render(request,"inicio.html",context)